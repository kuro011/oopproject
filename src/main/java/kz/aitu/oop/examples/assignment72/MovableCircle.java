package kz.aitu.oop.examples.assignment72;

public class MovableCircle extends MovablePoint{
    int radius;
    MovablePoint center;

    public MovableCircle(int x, int y, int xSpeed, int ySpeed, int radius) {
        super(x, y, xSpeed, ySpeed);
        this.radius = radius;
    }

    @Override
    public String toString() {
        return "radius=" + radius;
    }
}
