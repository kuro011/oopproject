package kz.aitu.oop.examples.polymorphism;

//Better implementing inheritance


public class Main {


    public static void main(String[] args) {


        Bird[] birds = new Bird[2];
        birds[0] = new Bird("bird1");
        birds[1] = new Bird("bird2");

        Dog[] dogs = new Dog[3];
        dogs[0] = new Dog("dog 1");
        dogs[1] = new Dog("dog 2");
        dogs[2] = new Dog("dog 3");

        for (Bird bird : birds) {
            System.out.println(bird.getName());
        }

        for (Dog dog : dogs) {
            System.out.println(dog.getName());
        }

        System.out.println("====================================");

        Animal[] animals = new Animal[5];
        animals[0] = (Animal)birds[0];
        animals[1] = (Animal)dogs[0];
        animals[2] = (Animal)birds[1];
        animals[3] = (Animal)dogs[1];
        animals[4] = (Animal)dogs[2];


        for (Animal animal : animals) {
            System.out.println("***********");
            System.out.println(animal.getName());

            animal.eat();

            if(animal instanceof Dog) {
                ((Dog)animal).run();
            }

            if(animal instanceof Bird) {
                ((Bird)animal).fly();
            }
        }


    }
}
