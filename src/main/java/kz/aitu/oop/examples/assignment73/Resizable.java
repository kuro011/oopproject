package kz.aitu.oop.examples.assignment73;

public interface Resizable {
    public void resize(int percent);
}
