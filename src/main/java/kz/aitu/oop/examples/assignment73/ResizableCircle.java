package kz.aitu.oop.examples.assignment73;

public class ResizableCircle extends Circle implements Resizable {
    public ResizableCircle(double radius){
        super(radius);
    }

    @Override
    public void resize(int percent){
        radius = radius*percent/100;
    }

    public String toString(){
        return "radius=" + radius;
    }
}
