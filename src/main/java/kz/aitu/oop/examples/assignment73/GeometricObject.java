package kz.aitu.oop.examples.assignment73;

public interface GeometricObject {
    public double getPerimeter();
    public double getArea();

}
