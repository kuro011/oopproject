package kz.aitu.oop.examples.assignment73;

public class Circle implements GeometricObject{
    protected double radius;

    public Circle(double radius) {
        this.radius = 1.0;
    }

    @Override
    public double getArea() {
        return Math.PI*radius*radius;
    }

    @Override
    public double getPerimeter() {
        return Math.PI*radius*2;
    }

    @Override
    public String toString() {
        return "radius=" + radius;
    }
}
