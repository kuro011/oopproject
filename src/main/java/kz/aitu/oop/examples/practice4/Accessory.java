package kz.aitu.oop.examples.practice4;

public class Accessory extends Good {
    private int accId;

    public Accessory(int id, int accId) {
        if(accId == 1){
            this.type = "Gravel";
            this.price = 20;
        }
        else if(accId == 2){
            this.type = "Artificial plant";
            this.price = 10;
        }
        else if(accId == 3){
            this.type = "Ornament";
            this.price = 35;
        }
        else{
            this.type = "Stones";
            this.price = 5;
        }
    }

    public int getAccId() {
        return accId;
    }

    public void setAccId(int accId) {
        this.accId = accId;
    }

    @Override
    public String toString() {
        return "Accessory{" +
                "accId=" + accId +
                ", id=" + id +
                ", type='" + type + '\'' +
                ", price=" + price +
                '}';
    }
}
