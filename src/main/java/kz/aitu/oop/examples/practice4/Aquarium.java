package kz.aitu.oop.examples.practice4;

import lombok.Data;
import java.util.LinkedList;

@Data
public class Aquarium {
    private int id;
    private double totalPrice;
    private LinkedList<Good> good;


    public Aquarium() {
        this.good = new LinkedList<>();
    }

    public void addGood(Good g){
        good.add(g);
        totalPrice = totalPrice + g.getPrice();
    }

    public void displayTotalPrice(){
        System.out.println("Total Price: " + totalPrice + "$\n");
    }

    @Override
    public String toString() {
        return "Aquarium{" +
                "id=" + id +
                ", totalPrice=" + totalPrice +
                ", good=" + good +
                '}';
    }
}
