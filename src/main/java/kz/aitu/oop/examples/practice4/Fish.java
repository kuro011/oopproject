package kz.aitu.oop.examples.practice4;

public class Fish extends Good {
    private int fishId;

    public Fish(int id, int fishId) {
        if(fishId == 1){
            this.type = "Molly Fish";
            this.price = 200;
        }
        else if(fishId == 2){
            this.type = "Guppies";
            this.price = 150;
        }
        else if(fishId == 3){
            this.type = "Tetras";
            this.price = 300;
        }
        else{
            this.type = "Betta Fish";
            this.price = 250;
        }
    }

    public int getFishId() {
        return fishId;
    }

    public void setFishId(int fishId) {
        this.fishId = fishId;
    }

    @Override
    public String toString() {
        return "Fish{" +
                "fishId=" + fishId +
                ", id=" + id +
                ", type='" + type + '\'' +
                ", price=" + price +
                '}';
    }
}
