package kz.aitu.oop.examples.practice4;

public class Reptile extends Good {
    private int repId;

    public Reptile(int id, int repId) {
        if(repId == 1){
            this.type = "Turtle";
            this.price = 300;
        }
        else if(repId == 2){
            this.type = "Bearded dragon";
            this.price = 500;
        }
        else if(repId == 3){
            this.type = "Water Dragon";
            this.price = 550;
        }
        else{
            this.type = "Small Lizard";
            this.price = 400;
        }
    }

    public int getRepId() {
        return repId;
    }

    public void setRepId(int repId) {
        this.repId = repId;
    }

    @Override
    public String toString() {
        return "Reptile{" +
                "repId=" + repId +
                ", id=" + id +
                ", type='" + type + '\'' +
                ", price=" + price +
                '}';
    }
}
