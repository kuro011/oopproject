package kz.aitu.oop.examples.practice4;

public class Main {
    public static void main(String[] args) {
        Fish f = new Fish(1, 3);
        Fish f1 = new Fish(2, 1);
        Reptile r = new Reptile(3, 4);
        Reptile r1 = new Reptile(4, 1);
        Accessory ac = new Accessory(5, 3);
        Accessory ac1 = new Accessory(6, 1);

        Aquarium aq = new Aquarium();
        aq.addGood(f);
        aq.addGood(f1);
        aq.addGood(r);
        aq.addGood(r1);
        aq.addGood(ac);
        aq.addGood(ac1);
        aq.setId(1);
        aq.displayTotalPrice();
        System.out.println(aq);
    }
}
