package kz.aitu.oop.examples.practice4;

import lombok.Data;

@Data
public class Good {
    protected int id;
    protected String type;
    protected double price;

    @Override
    public String toString() {
        return "Good{" +
                "id=" + id +
                ", type='" + type + '\'' +
                ", price=" + price +
                '}';
    }
}
