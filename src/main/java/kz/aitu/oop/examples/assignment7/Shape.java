package kz.aitu.oop.examples.assignment7;

public abstract class Shape {
    protected String color;
    protected boolean filled;

    public Shape() {
        this.color = "green";
        this.filled = true;
    }

    public Shape(String color, boolean filled) {
        this.color = color;
        this.filled = filled;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public boolean isFilled() {
        return filled;
    }

    public void setFilled(boolean filled) {
        this.filled = filled;
    }

    abstract double getArea();
    abstract double getPerimeter();

    public String toString(){
        String ansFilled;
        if (isFilled()){
            ansFilled = "filled";
        }
        else {
            ansFilled = "Not filled";
        }
        return "A Shape with color of " + getColor() + " and " + ansFilled;
    }

}
