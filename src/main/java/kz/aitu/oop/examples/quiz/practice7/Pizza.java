package kz.aitu.oop.examples.quiz.practice7;

public class Pizza implements Food {
    @Override
    public void getType() {
        System.out.println("The factory returned class Pizza");
        System.out.println("Someone ordered Fast Food!");
    }
}
