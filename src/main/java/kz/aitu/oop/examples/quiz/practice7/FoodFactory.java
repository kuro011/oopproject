package kz.aitu.oop.examples.quiz.practice7;

public class FoodFactory {
    public Food getFood(String food){
        if(food.equalsIgnoreCase("cake")){
            return new Cake();
        }
        if(food.equalsIgnoreCase("pizza")){
            return new Pizza();
        }
        return null;
    }
}
