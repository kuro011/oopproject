package kz.aitu.oop.examples.quiz.practice6;

public class Singleton {
    private Singleton(){}
    public String str;

    public static Singleton singleInstance = null;

    public static Singleton getSingleInstance(){
        if(singleInstance == null){
            return singleInstance = new Singleton();
        }
        return singleInstance;
    }


    @Override
    public String toString() {
        return "Hello I am a singleton! Let me say " + str + " to you";
    }
}
