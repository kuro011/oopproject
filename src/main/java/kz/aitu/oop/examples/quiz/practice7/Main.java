package kz.aitu.oop.examples.quiz.practice7;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        FoodFactory fF = new FoodFactory();
        Scanner input = new Scanner(System.in);
        String type = input.nextLine();
        Food f = fF.getFood(type);
        f.getType();
    }

}
