package kz.aitu.oop.examples.quiz.practice6;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        Singleton s = Singleton.getSingleInstance();
        s.str = input.nextLine();

        Singleton s1 = Singleton.getSingleInstance();
        s1.str = input.nextLine();

        System.out.println(s.toString());
        System.out.println(s1.toString());
    }
}
