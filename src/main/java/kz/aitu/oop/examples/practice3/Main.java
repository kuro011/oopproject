package kz.aitu.oop.examples.practice3;

public class Main {
    public static void main(String[] args) {
        DataScientist ds = new DataScientist();
        ds.id = 1;
        ds.name = "Alikhan";
        ProjectManager pm = new ProjectManager();
        pm.id=2;
        pm.name="Damir";

        Project pr = new Project();
        pr.addEmp(pm);
        pr.addEmp(ds);
        pr.setId(2);
        pr.setName("Astana");
        System.out.println(pr);
        pr.displayTotalCost();
    }
}
