package kz.aitu.oop.examples.practice3;

import lombok.Data;

@Data
public class Employee {
    protected int id;
    protected String name;
    protected String job;
    protected double salary;

    @Override
    public String toString() {
        return "Employee{" +"id=" + id +
                ", name='" + name + '\''+
                ", job='" + job + '\'' +
                ", salary=" + salary +
                '}' + "\n";
    }
}
