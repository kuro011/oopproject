package kz.aitu.oop.examples.practice3;

import lombok.Data;
import java.util.LinkedList;

@Data
public class Project {
    private int id;
    private String name;
    private double totalCost;
    private LinkedList<Employee> emp;

    public Project() {
        this.emp = new LinkedList<>();
    }

    public void addEmp(Employee employee) {
        emp.add(employee);
        totalCost = totalCost + employee.getSalary();
    }

    public void displayTotalCost() {
        System.out.println("Total Cost: " + totalCost + "$\n");
    }


    @Override
    public String toString() {
        return "Project{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", totalCost=" + totalCost +
                ", \nemp=" + emp +
                '}';
    }
}
