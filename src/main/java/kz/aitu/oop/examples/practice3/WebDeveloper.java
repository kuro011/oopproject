package kz.aitu.oop.examples.practice3;

public class WebDeveloper extends Employee {

    public WebDeveloper() {
        job = "Web Developer";
        salary = 2000;
    }


    @Override
    public String toString() {
        return "WebDeveloper{" +"id=" + id +
                ", name='" + name + '\''+
                ", job='" + job + '\'' +
                ", salary=" + salary +
                '}' + "\n";
    }
}
