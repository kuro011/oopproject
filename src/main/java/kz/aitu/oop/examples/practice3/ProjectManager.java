package kz.aitu.oop.examples.practice3;

public class ProjectManager extends Employee{
    public ProjectManager() {
        job = "Project Manager";
        salary = 5000;
    }

    @Override
    public String toString() {
        return "\nProjectManager{" +"id=" + id +
                ", name='" + name + '\''+
                ", job='" + job + '\'' +
                ", salary=" + salary +
                '}' + "\n";
    }
}
