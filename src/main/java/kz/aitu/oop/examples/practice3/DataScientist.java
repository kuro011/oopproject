package kz.aitu.oop.examples.practice3;

public class DataScientist extends Employee {
    public DataScientist() {
        job = "Data Scientist";
        salary = 3500;
    }

    @Override
    public String toString() {
        return "\nDataScientist{" +"id=" + id +
                ", name='" + name + '\''+
                ", job='" + job + '\'' +
                ", salary=" + salary +
                '}' + "\n";
    }
}
