package kz.aitu.oop.examples.practice3;

public class SoftwareEngineer extends Employee {
    public SoftwareEngineer() {
        job = "Software Engineer";
        salary = 3000;
    }


    @Override
    public String toString() {
        return "SoftwareEngineer{" +"id=" + id +
                ", name='" + name + '\''+
                ", job='" + job + '\'' +
                ", salary=" + salary +
                '}' + "\n";
    }
}
