package kz.aitu.oop.examples.practice5;

public class Precious extends Stone {
    private String name;

    public Precious(int id, double weight, String name) {
        this.id = id;
        this.name = name;
        this.weight = weight;
        switch (name) {
            case "diamond":
                this.cost = weight * 500000;
                break;
            case "sapphire":
                this.cost = weight * 300000;
                break;
            case "ruby":
                this.cost = weight * 400000;
                break;
            default:
                this.cost = weight * 350000;
                break;
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Precious{" +
                "name='" + name + '\'' +
                ", id=" + id +
                ", weight=" + weight +
                ", cost=" + cost +
                '}';
    }
}
