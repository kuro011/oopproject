package kz.aitu.oop.examples.practice5;

import lombok.Data;

@Data
public class Stone {
    protected int id;
    protected double weight;
    protected double cost;

    @Override
    public String toString() {
        return "Stone{" +
                "id=" + id +
                ", weight=" + weight +
                ", cost=" + cost +
                '}';
    }
}
