package kz.aitu.oop.examples.practice5;

public class SemiPrecious extends Stone {
    private String name;
    public SemiPrecious(int id, double weight, String name) {
        this.id = id;
        this.name = name;
        this.weight = weight;
        switch (name) {
            case "amethyst":
                this.cost = weight * 50000;
                break;
            case "onyx":
                this.cost = weight * 90000;
                break;
            case "coral":
                this.cost = weight * 10000;
                break;
            default:
                this.cost = weight * 25000;
                break;
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "SemiPrecious{" +
                "id=" + id +
                ", weight=" + weight +
                ", cost=" + cost +
                '}';
    }
}
