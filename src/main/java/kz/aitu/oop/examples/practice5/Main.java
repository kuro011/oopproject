package kz.aitu.oop.examples.practice5;

public class Main {
    public static void main(String[] args) {
        Precious pr = new Precious(1, 3, "diamond");
        SemiPrecious sp = new SemiPrecious(2, 4, "amazonite");
        Precious pr1 = new Precious(3, 6, "sapphire");
        Necklace n = new Necklace();
        n.addStone(pr);
        n.addStone(pr1);
        n.addStone(sp);
        n.displayTotalWeight();
        n.displayTotalCost();
        System.out.println(n);
    }
}
