package kz.aitu.oop.examples.practice5;

import kz.aitu.oop.examples.practice4.Good;
import lombok.Data;
import java.util.LinkedList;

@Data
public class Necklace {
    private int id;
    private double totalWeight;
    private double totalCost;
    private LinkedList<Stone> stone;


    public Necklace() {
        this.stone = new LinkedList<>();
    }

    public void addStone(Stone s){
        stone.add(s);
        totalCost = totalCost + s.getCost();
        totalWeight = totalWeight + s.getWeight();
    }

    public void displayTotalWeight(){
        System.out.println("Total Weight: " + totalWeight + " carats\n");
    }

    public void displayTotalCost(){
        System.out.println("Total Cost: " + totalCost + "kzt\n");
    }

    @Override
    public String toString() {
        return "Necklace{totalWeight=" + totalWeight +
                ", totalCost=" + totalCost +
                ", stone=" + stone +
                '}';
    }
}
