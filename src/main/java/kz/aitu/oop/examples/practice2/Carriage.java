package kz.aitu.oop.examples.practice2;

import lombok.Data;
import java.util.LinkedList;

@Data
public class Carriage {
    private int id;
    private LinkedList<Passenger> passengerList;
    private double capacity;

    public Carriage(int id) {
        this.id = id;
        this.passengerList = new LinkedList<>();
        this.capacity = 2000;
    }

    public void addPass(Passenger pas){
        passengerList.add(pas);
    }

    public int carPassNum(){
        return passengerList.size();
    }
}
