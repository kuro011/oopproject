package kz.aitu.oop.examples.practice2;

import lombok.Data;

import java.util.LinkedList;

@Data
public class Train {
    private LinkedList<Carriage> capacityList;

    public Train() {
        this.capacityList = new LinkedList<>();
    }

    public void addCar(Carriage car){
        capacityList.add(car);
    }

    public int carCap(){
        return capacityList.size();
    }

    public void displayTotalCapacity(Carriage car, Locomotive loc, Train train){
        double t =  car.getCapacity()*train.carCap() + loc.getCapacity();
        System.out.println("Total capacity of the train " + t);
    }

    public void displayTotalPassNum(Carriage car){
        System.out.println("Total number of passengers " + car.carPassNum());
    }

}

