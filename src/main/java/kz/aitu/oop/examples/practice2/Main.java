package kz.aitu.oop.examples.practice2;

public class Main {
    public static void main(String[] args) {
        Passenger p = new Passenger(1, "Alikhan", 20);

        Carriage c = new Carriage(1);
        c.addPass(p);
        c.setCapacity(2000);

        Carriage c1 = new Carriage(2);
        c1.addPass(p);

        Locomotive l = new Locomotive();
        l.setId(643);
        l.setType("Talgo");
        l.setCapacity(1000);

        Train t = new Train();
        t.addCar(c);
        t.addCar(c1);

        t.displayTotalCapacity(c,l,t);
        t.displayTotalPassNum(c);
    }
}
