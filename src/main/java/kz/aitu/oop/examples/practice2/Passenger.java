package kz.aitu.oop.examples.practice2;

import lombok.Data;

@Data
public class Passenger {
    private int id;
    private String name;
    private int age;

    public Passenger(int id, String name, int age) {
        this.id = id;
        this.name = name;
        this.age = age;
    }

}
