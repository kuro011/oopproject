package kz.aitu.oop.examples.inheritance;

public class Main {

    public static void main(String[] args) {
        Eagle eagle = new Eagle("egg", "feather", "eagle", 15);

        System.out.println(eagle.getReproduction());
    }
}
